import pygame, sys
from pygame import mixer
import pygame_menu
import random

pygame.init()

# create gameDisplay
display_width = 700
display_height = 600
gameDisplay = pygame.display.set_mode((display_width, display_height))

# background
gameDisplay.fill((0, 0, 0))
background = pygame.image.load('background.png')
gameDisplay.blit(background, (0, 0))

# title
pygame.display.set_caption("Space Invaders")

# icon
gameIcon = pygame.image.load('ufon.png')
pygame.display.set_icon(gameIcon)

# gamer
gamerImg = pygame.image.load('space_ship.png')

# enemy
enemyImg = pygame.image.load('enemy.png')

# explosion
explosionImg = pygame.image.load('explosion.png')

# score
font = pygame.font.Font('freesansbold.ttf', 32)
scoreX = 280
scoreY = 10

# game Over
over_font = pygame.font.Font('freesansbold.ttf', 64)

# sound
mixer.music.load("background.wav")
mixer.music.play(-1)

def initGame():
    global gamerX, gamerY, enemyX, enemyY, explosionX, explosionY, explosionTransparency, projectileX, projectileY, score_value
    # gamer
    gamerX = 315
    gamerY = 520

    # enemy
    enemyX = random.randint(16,652)
    enemyY = -32

    # explosion
    explosionX = 0
    explosionY = -32
    explosionTransparency = 0

    # projectile
    projectileX = 0
    projectileY = -4

    # score
    score_value = 0

def clear ():
    gameDisplay.blit(background, (0, 0))

def gamer ():
    gameDisplay.blit(gamerImg, (gamerX, gamerY))

def enemy ():
    gameDisplay.blit(enemyImg, (enemyX, enemyY))

def projectile ():
    pygame.draw.circle(gameDisplay,(255,255,255),(projectileX,projectileY),5,0)

def explosion ():
    gameDisplay.blit(explosionImg, (explosionX, explosionY))

def show_score(x, y):
    score = font.render("Score : " + str(score_value), True, (255, 255, 255))
    gameDisplay.blit(score, (x, y))

def game_over_text():
    over_text = over_font.render("GAME OVER", True, (255, 255, 255))
    gameDisplay.blit(over_text, (150, 250))

def start_the_game():
    global gamerX, projectileY, enemyX, enemyY, explosionX, explosionY, explosionTransparency, projectileX, projectileY, score_value
    moveRight = False
    moveLeft = False
    running = True
    gameOver = False
    initGame()
    while running:
        clear()        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                if event.key == pygame.K_LEFT:
                    moveLeft = True
                if event.key == pygame.K_RIGHT:
                    moveRight = True
                if event.key == pygame.K_SPACE and projectileY < 0:
                    if gameOver:
                        running = False
                    else:
                        bulletSound = mixer.Sound("shoot.wav")
                        bulletSound.play()
                        projectileX = gamerX + 32
                        projectileY = 530
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT:
                    moveLeft = False
                if event.key == pygame.K_RIGHT:
                    moveRight = False
        if not gameOver:
            if moveRight:
                gamerX += 4
            if moveLeft:
                gamerX -= 4
            # speed movement
            projectileY -= 7
            enemyY += 3
        
            
        # boundaries
        if gamerX <= 0:
            gamerX = 0
        if gamerX >= 636:
            gamerX = 636
    
        # collision
        elif projectileX > enemyX and projectileX < (enemyX+32) and projectileY > enemyY and projectileY < (enemyY+32):
            explosionX = enemyX
            explosionY = enemyY
            explosionTransparency = 255
            explosionSound = mixer.Sound("explosion.wav")
            explosionSound.play()
            score_value += 1
            enemyY = -32
            enemyX = random.randint(16,652)
        explosionImg.set_alpha(explosionTransparency)
        explosionTransparency -= 5
        # game over
        if enemyY > 550:
            gameOver = True

        if gameOver:
            game_over_text()
            return pygame_menu
        gamer()
        enemy()
        projectile()
        explosion()
        show_score(scoreX, scoreY)
    
        pygame.display.update()
                
# main menu
menu = pygame_menu.Menu(300, 400, 'Space Invaders',
                       theme=pygame_menu.themes.THEME_DARK)

menu.add_button('Play', start_the_game)
menu.add_button('Quit', pygame_menu.events.EXIT)

menu.mainloop(gameDisplay)